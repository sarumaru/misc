# misc

## Icon images

base monkey image from [File:Young Barbary Ape sitting on Plant Pot.jpg - Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Young_Barbary_Ape_sitting_on_Plant_Pot.jpg)[PD]

* icon.svg
* icon1.png
* icon2.png

## Licenses

Licensed under the Creative Commons [Attribution 4.0 International](http://creativecommons.org/licenses/by/4.0/) license.
